//
// Swap values
// https://cpppatterns.com/patterns/swap-values.html
//
// Swap the values of 2 objects.
//
// Even though we "import" the `std::swap()`, we call the unqualified version of
// `swap()`.  This allows a user-defined specialization of `swap()` to be found,
// based on the ADL rules, which may provide a more efficient implementation,
// before falling back to the generic default.
//
#include <iostream>
#include <string>
#include <utility>

int main()
{
   std::string s1 = "World";
   std::string s2 = "Hello";

   using std::swap;
   swap(s1, s2);

   std::cout << s1 << ", " << s2 << "!\n";
   return 0;
}
