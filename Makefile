##
## Makefile
## General rules for building sample C++ programs
##

SHELL		:= /bin/sh

CXXFLAGS	+= -Wall -Wextra ${CFLAGS}

SOURCES		:= $(wildcard *.cpp *.cc)
TARGETS		:= $(basename ${SOURCES})

all: build
help:
	@echo 'build/manage trivial C++ programs'
	@echo
	@echo 'available rules are:'
	@echo '   help      - this message'
	@echo '   build     - build defined programs'
	@echo '   test      - run any available tests'
	@echo '   clean     - remove object files'
	@echo '   cleanall  - remove all generated files'
	@echo '   veryclean - remove all generated files'
clean:
	$(RM) *.o
cleanall: clean
	$(RM) ${TARGETS}
veryclean: cleanall
build: ${TARGETS}
test:
	@echo 'nothing to test for now'

##
## Manual dependencies
##

##
## Rules and custom functions
###

do_link	= $(LINK.cc) $^ $(LOADLIBES) $(LDLIBS) -o $@

.PHONY: all help build clean veryclean cleanall test
