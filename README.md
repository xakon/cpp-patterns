C++ Patterns
============

A collection of C++ samples as found at the official web site.


Resources
---------

 - [C++ Samples](http://www.cppsamples.com/)
 - [C++ Patterns](http://cpppatterns.com/)
